from functools import wraps
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Callable, Literal, Optional

from pandas import DataFrame, Series, concat
import vcfpy

from src.feature_extraction.step1.filter_vcf import (
    filter_synonymous_variants,
    get_annotations,
    get_annotation_value,
    is_frameshift,
    is_non_synonymous,
)
from src.feature_extraction.step2.extract import (
    extract_copy_number_spectra,
    extract_mutation_spectra,
)
from src.feature_extraction.step3.transform import CosmicNMF


_POSSIBLE_SBS_SEQUENCING_ARTEFACTS = [
    # Extracted from https://cancer.sanger.ac.uk/signatures/sbs/.
    "SBS27",
    "SBS43",
    "SBS45",
    "SBS46",
    "SBS47",
    "SBS48",
    "SBS49",
    "SBS50",
    "SBS51",
    "SBS52",
    "SBS53",
    "SBS54",
    "SBS55",
    "SBS56",
    "SBS57",
    "SBS58",
    "SBS59",
    "SBS60",
]

_POSSIBLE_CN_SEQUENCING_ARTEFACTS = ["CN22", "CN23", "CN24"]


def non_synonymous(method):
    """Decorator that pre-filters synonymous variants from the VCF file."""

    @wraps(method)
    def decorated_transform(input_vcf: Path, *args, **kwargs):
        with TemporaryDirectory() as tmp_dir:
            target_vcf = Path(tmp_dir) / input_vcf.name.replace(".gz", "")
            filter_synonymous_variants(input_vcf, target_vcf, soft=False)
            x_transformed = method(target_vcf, *args, **kwargs)
        return x_transformed

    return decorated_transform


def spectrum(vcf_file: Path, exome: bool = False) -> DataFrame:
    """Extract single + doublet base substitutions and indel spectra from VCF file."""
    mutation_matrices = extract_mutation_spectra(vcf_file, exome=exome)
    spectra = list(mutation_matrices.values())
    return concat(spectra, axis="columns")


@non_synonymous
def non_synonymous_spectrum(vcf_file: Path, exome: bool = False) -> DataFrame:
    return spectrum(vcf_file, exome)


def mutational_signature(
    vcf_file: Path, filter_seq_artefact_signatures: bool = True
) -> DataFrame:
    """
    Extract mutation spectra and deconvolute into COSMIC signatures.

    Assumes that variants are from the GRCh37 reference genome.

    Args:
        vcf_file: File to analyse.
        filter_seq_artefact_signatures: When True, remove mutational
            signatures that are potentially related to sequencing artefacts.
    """
    mutation_matrices = extract_mutation_spectra(vcf_file, exome=False)
    signatures = []
    for spectrum in [
        "single_base_substitutions",
        "doublet_base_substitutions",
        "indel",
    ]:
        decomposer = CosmicNMF(cosmic_signature=spectrum)
        signatures.append(decomposer.transform(mutation_matrices[spectrum]))

    result = concat(signatures, axis="columns")
    if filter_seq_artefact_signatures:
        return result.drop(columns=_POSSIBLE_SBS_SEQUENCING_ARTEFACTS)
    return result


@non_synonymous
def non_synonymous_mutational_signature(
    vcf_file: Path, filter_seq_artefact_signatures: bool = True
) -> DataFrame:
    return mutational_signature(vcf_file, filter_seq_artefact_signatures)


def copy_number_spectrum(cnv_file: Path) -> DataFrame:
    """Only bucketize copy number segments into 48 feature spectrum."""
    return extract_copy_number_spectra(cnv_file)


def copy_number_signature(
    cnv_file: Path, filter_seq_artefact_signatures: bool = True
) -> DataFrame:
    """Deconvolute copy number spectra into COSMIC copy number signatures."""
    X_cn = extract_copy_number_spectra(cnv_file)
    decoder = CosmicNMF(cosmic_signature="cnv")
    X_signatures = decoder.transform(X_cn)
    if filter_seq_artefact_signatures:
        return X_signatures.drop(columns=_POSSIBLE_CN_SEQUENCING_ARTEFACTS)
    return X_signatures


def tumor_mutational_burden(
    input_vcf: Path, stratify: Optional[str | int] = None
) -> DataFrame:
    """Count number of non-synonymous variants.

    Args:
        stratify: When specified, aggregate by this `ANN` annotation field (e.g.,
            Gene ID, field number 3).

    Returns:
        A multi-index dataframe with where the second level contains the number of
        mutations and frameshifts per gene.
    """
    index = input_vcf.name.split(".")[0]
    with vcfpy.Reader.from_path(str(input_vcf)) as reader:

        items = []
        for record in reader:
            try:
                annotations = get_annotations(record, reader)
            except KeyError:
                # Filter variants without annotation.
                continue
            else:
                # Filter variants that are not non-synonymous.
                if not any(map(is_non_synonymous, annotations)):
                    continue

            # When no stratification is used, aggregate by unique stratum '_dummy'.
            aggregation_key = "_dummy"
            if stratify is not None:
                aggregation_key = get_annotation_value(annotations, key=stratify)
                assert aggregation_key != "_dummy"

            if any(map(is_frameshift, annotations)):
                # 1 frame shift, 1 non-synonymous (because frameshift is non-synonymous).
                items.append([aggregation_key, 1, 1])
            else:
                items.append([aggregation_key, 1, 0])

    df = (
        DataFrame(items, columns=["stratum", "tmb", "frameshifts"])
        .groupby("stratum")
        .sum()
    )
    df = concat({index: df}, names=["Patient ID"])
    # Peel off '_dummy' stratum when no stratification is to be done.
    if stratify is None:
        df.index = df.index.droplevel(1)
        return df
    return df


def stratified_tumor_mutational_burden(
    input_vcf: Path, vocab: set, stratum: str | int = 3
) -> DataFrame:
    """Estimate tumor mutational burden per stratum.

    Args:
        vocab: Stratify tumor mutational burden by elements in `vocab`. All others
            mutations are grouped as out-of-vocabulary (OOV) variants.

    Returns:
        A dataframe with the tumor mutational burden per stratum along the columns.
    """

    x_i = tumor_mutational_burden(input_vcf, stratify=stratum)["tmb"]
    index = x_i.index.get_level_values("Patient ID")[0]

    def vocab_encode_stratum(idx):
        if idx in vocab:
            return idx
        return "oov"

    x_coarse = x_i.loc[index].groupby(vocab_encode_stratum).sum()

    total_vocab = sorted(vocab) + ["oov"]
    x_tmb = DataFrame(0, index=[index], columns=total_vocab)
    x_tmb.loc[index, x_coarse.index] = x_coarse

    return x_tmb


class VariantDataGenerator:
    """Transform (unstructured) mutation files to tabular format."""

    def __init__(
        self,
        transform: Callable,
    ):
        """
        Generate tabular data from files containing variant or copy number calls.

        Args:
            transform: Method that generates a row from a VCF or CNV file.
        """
        self.transform = transform

    def flow_from_dataframe(
        self,
        dataframe: DataFrame,
        x_col="filename",
        y_col="class",
        class_mode: Optional[Literal["raw"]] = "raw",
        keep_columns: bool = True,
        coverage_size: Optional[float | str] = None,
        decimals: Optional[int] = 2,
    ):
        """Load dataset by reading VCF files and target label from dataframe.

        Args:
            dataframe: Pandas dataframe with columns pointing to VCF or CNV files.
            class_mode: When None, don't extract target label (inference mode).
            x_col: Column pointing to location of VCF or CNV file.
            y_col: Target label column.
            class_mode: Return features and labels during training mode ("raw"),
                or return only features during serving (None).
            keep_columns: Use only the features extracted through `x_col` (False) or
                also concatenate other columns in the dataframe after extraction (True).
            coverage_size: Normalise estimates by value (float), column (str), or not at
                all (None). Usually, this value is the size of the genomic region
                [typically in megabases (mb)] covered at sufficient depth to call
                variants.
            decimals: If not None, round to this many decimals.

        Returns: When class_mode is `None` return features `X`, otherwise return a
            pair (X, y) with labels `y`.

        """
        if keep_columns:
            to_keep = dataframe.columns.difference([x_col, y_col])

        if coverage_size is None:
            normalisation = Series(1, index=dataframe.index)
        elif isinstance(coverage_size, (float, int)):
            normalisation = Series(coverage_size, index=dataframe.index)
        elif coverage_size in dataframe.columns:
            normalisation = dataframe[coverage_size]
        else:
            raise KeyError(f"Unknown column {coverage_size}.")

        X = []
        # Loop trough all label directories.
        for index, filename in dataframe[x_col].iteritems():
            x_i = self._transform_x(Path(filename))
            # Normalise estimate by exome size.
            x_i /= normalisation[index]

            # Concatenate other columns.
            if keep_columns:
                if x_i.index.nlevels > 1:
                    raise ValueError(
                        "Unable to coalesce single-index data frame with multi-index result from `transform`."
                    )
                if to_keep.size > 0:
                    x_passthrough = dataframe.loc[[index], to_keep]
                    x_i.index = [index]
                    x_i = concat([x_i, x_passthrough], axis="columns")

            X.append(x_i)

        X_data_frame = concat(X, axis="rows")
        # Use index names from original data frame (instead of that given by
        # `transform`).
        if X_data_frame.index.nlevels > 1:
            X_data_frame.index = X_data_frame.index.set_levels(dataframe.index, level=0)
        else:
            X_data_frame.index = dataframe.index

        if class_mode is None:
            return X_data_frame

        y = dataframe[y_col].copy()

        # Don't round when None.
        if decimals is None:
            return X_data_frame, y
        return X_data_frame.round(decimals), y

    def _transform_x(self, input_vcf: Path) -> Series:
        """Transform single record (e.g., a VCF or copy number file)."""
        return self.transform(input_vcf)
