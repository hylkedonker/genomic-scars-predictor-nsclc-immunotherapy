from datetime import datetime
from pathlib import Path
import re
from typing import Optional, Union
import warnings

from pandas import NA, notna, read_csv
import vcfpy
from vcfpy import DEL, INS, SNV, MNV, Header, HeaderLine, SamplesInfos, Substitution
from vcfpy.exceptions import FieldInfoNotFound, VCFPyException


def check_vcf_ann_header(reader: vcfpy.Reader):
    """
    Check header to see if INFO column has variant annotation `ANN` fields.

    Raises:
        VCFPyException: When header specifies no ANN field  or incorrect ANN keys.
    """
    with warnings.catch_warnings():
        warnings.filterwarnings("error")
        try:
            annotation = reader.header.get_info_field_info("ANN")
        except FieldInfoNotFound:
            raise VCFPyException("Missing ANN field in VCF.")

    return annotation


def get_annotations_keys(reader: vcfpy.Reader) -> list:
    """
    Get the annotation's data fields (=keys).

    This corresponds to the data fields in the value corresponding to the
    `ANN` key in the `INFO`field.

    Returns:
        Keys corresponding to record ANN array.
    """
    annotation = check_vcf_ann_header(reader)
    ann_format = annotation.description
    mo = re.search("'(.+)'", ann_format)
    ann_keys = [key.strip() for key in mo.group(1).split("|")]

    # According to standard, there are 16 annotations data fields.
    assert len(ann_keys) == 16, "Variant annotations not compatible with standard."

    return ann_keys


def get_annotations(record: vcfpy.Record, reader: vcfpy.Reader) -> list[dict]:
    """Turn annotations data field into dictionary.

    Args:
        record: Single record in the VCF file.
        reader: VCFpy `Reader` istance with header information.

    Returns:
        Annotation dictionary per allele.
    """

    ann_keys = get_annotations_keys(reader)
    return [
        dict(zip(ann_keys, ann_values.split("|"))) for ann_values in record.INFO["ANN"]
    ]


def is_frameshift(annotation: dict) -> bool:
    """Test if an annotated variant is a frameshift."""
    field_names = tuple(annotation.keys())

    # The standard for the annotation field can be found here:
    # https://pcingola.github.io/SnpEff/adds/VCFannotationformat_v1.0.pdf
    annotation_field = field_names[1]
    hgsvp_field = field_names[11]

    # Only variants affecting protein.
    if annotation[hgsvp_field] != "":
        # Only variants that are marked as a frame shift.
        if annotation[annotation_field] == "frameshift_variant":
            return True
    return False


def get_annotation_value(annotations, key: Union[str, int]) -> Optional[str]:
    """From all variant annotations, extract field `key`.

    Args:
        key: When `int`, use as array index to extract value. When `str`, assume it is
            a key in the dict.
    """

    def _get_key(from_dict):
        """Parse type of key and extract accordingly."""
        if isinstance(key, int):
            return tuple(from_dict.values())[key]
        return from_dict[key]

    values = (_get_key(ann) for ann in annotations)
    try:
        return next(filter(lambda x: x != "", values))
    except StopIteration:
        return NA


def is_non_synonymous(annotation: dict) -> bool:
    """Test if an annotated variant is non-synonymous.

    Args:
        annotation: dict where keys and values are according to ANN standard.
    """
    field_names = tuple(annotation.keys())

    # The standard for the annotation field can be found here:
    # https://pcingola.github.io/SnpEff/adds/VCFannotationformat_v1.0.pdf
    annotation_field = field_names[1]
    hgsvp_field = field_names[10]
    # Only variants affecting protein.
    if annotation[hgsvp_field] != "":
        # Only variants that change amino-acid sequence.
        var_annot_values = annotation[annotation_field].split("&")
        synonymous_type = (
            "synonymous_variant",
            "stop_retained_variant",
            "start_retained_variant",
        )
        non_synonymous = all(v.strip() not in synonymous_type for v in var_annot_values)
        if non_synonymous:
            return True
    return False


def filter_synonymous_variants(input_vcf: Path, filtered_vcf: Path, soft: bool = False):
    """
    Filter synonymous (non-amino acid sequencing changing) variants.

    Args:
        input_vcf: Variant file (possbily gzipped) to filter.
        filtered_vcf: Store filtered VCF to this (possbily gzipped) file.
        soft: When `True`, apply a soft filter that only changes the FILTER
            column (but keeps the record). When `False`, remove filtered
            records.

    Raises:
        VCFPyException: When header specifies no `ANN` field  or incorrect `ANN`
            keys.
    """
    with vcfpy.Reader.from_path(str(input_vcf)) as reader:
        # Soft filter adds the following variant FILTER.
        if soft:
            reader.header.add_filter_line(
                vcfpy.OrderedDict(
                    [
                        ("ID", "IS_SYNON"),
                        (
                            "Description",
                            "The variant is not amino-acid sequence changing.",
                        ),
                    ]
                )
            )

        with vcfpy.Writer.from_path(filtered_vcf, reader.header) as writer:
            for record in reader:
                try:
                    annotations = get_annotations(record, reader)
                except KeyError:
                    # Filter variants without annotation.
                    if not soft:
                        continue
                    record.add_filter("IS_SYNON")
                else:
                    # Filter variants that are not non-synonymous.
                    if not any(map(is_non_synonymous, annotations)):
                        if not soft:
                            continue
                        record.add_filter("IS_SYNON")

                writer.write_record(record)
