from pathlib import Path
import sys
from tempfile import TemporaryDirectory
from typing import Optional

from numpy import array, isin
import pandas as pd
from pandas import DataFrame, Series
from sklearn.base import BaseEstimator
from sklearn.feature_selection import SelectorMixin
import vcfpy

from src.feature_extraction.step1.filter_vcf import (
    filter_synonymous_variants,
    get_annotations,
)
from src.utils import (
    pyrimidine_doublet_base_substitution,
    pyrimidine_single_base_substitution,
)


def allele_frequency(alt_depth, total_depth):
    result = Series(0, index=alt_depth.index)
    non_zero = alt_depth > 0
    result[non_zero] = alt_depth[non_zero] / total_depth[non_zero]
    return result


def extract_rna_vcf_spectrum(input_vcf: str) -> DataFrame:
    """Read RNA coverage from VCF file.

    N.B., these VCF files contain
    - DP: sequencing coverage depth of RNA.
    - AF: The allele frequency of the variants based on RNA reads.
    """
    # 1) Perform extra synonymous filter, in case the VCF still contains
    # silent mutations.
    with TemporaryDirectory() as tmpdir:
        output_vcf = Path(tmpdir) / Path(input_vcf).name

        filter_synonymous_variants(Path(input_vcf), output_vcf, soft=False)

        # 2) Load VCF file as DataFrame with the follow-fields.
        header = [
            "chrom",
            "pos",
            "ref",
            "alt",
            "n_alt",
            "n_depth",
            "transcript",
            "spectrum",
        ]
        rows = []
        with vcfpy.Reader.from_path(str(output_vcf)) as reader:
            for record in reader:
                total_depth = record.INFO["DP"]
                annotations = get_annotations(record, reader)
                for i, vaf in enumerate(record.INFO["AF"]):
                    ann_i = annotations[i]
                    transcript = ann_i["Gene_ID"]
                    alt = record.ALT[i].value
                    if len(record.REF) == 1 and len(alt) == 1:
                        # The TNC contains reference genome, not germline variant.
                        triplet = (
                            record.INFO["TNC"][0] + record.REF + record.INFO["TNC"][2]
                        )
                        spectrum = pyrimidine_single_base_substitution(triplet, alt)
                    elif len(record.REF) == 2 and len(alt) == 2:
                        spectrum = pyrimidine_doublet_base_substitution(record.REF, alt)
                    # Ignore indels, they were not significant.
                    else:
                        continue
                    rows.append(
                        [
                            record.CHROM,
                            record.POS,
                            record.REF,
                            alt,
                            total_depth * vaf,
                            total_depth,
                            transcript,
                            spectrum,
                        ]
                    )

    # 3) Pool reads and re-estimate VAF.
    data = DataFrame(rows, columns=header)
    data["vaf"] = allele_frequency(data["n_alt"], data["n_depth"])
    return data


class PassthroughTransformer(BaseEstimator, SelectorMixin):
    """Preprocessing wrapper that always keeps a selected set of features."""

    def __init__(
        self,
        passthrough: list = [],
        verbose: bool = False,
    ):
        self.passthrough = passthrough
        self.verbose = verbose

    def fit(self, X: DataFrame, y=None):
        """Determine which columns to actually passthrough unfiltered."""
        self._validate_data(X, y)

        if not isinstance(X, DataFrame):
            raise TypeError("Expects pandas dataframe as input.")

        # Allow for missing passthrough columns.
        candidates = set(self.passthrough).intersection(X.columns)
        self.passthrough_: list = sorted(candidates)
        return self

    def _get_support_mask(self, passthrough: bool = True):
        """Compute support mask of features.

        Args:
            passthrough: When `False`, only unmask selected columns (filter
                whitelisted passthrough features).
        """
        features = self.feature_names_in_
        if passthrough:
            to_select = self.selected_columns_ + self.passthrough_
            return isin(features, array(to_select, dtype=object))
        return isin(features, self.selected_columns_)

    def transform(self, X: DataFrame, y=None):
        """Filter all but the selected features."""
        columns = sorted(
            set(X.columns).intersection(self.selected_columns_ + self.passthrough_)
        )
        if len(columns) < len(self.selected_columns_ + self.passthrough_):
            dropped_columns = set(self.selected_columns_ + self.passthrough_) - set(
                columns
            )
            if self.verbose:
                print(
                    (
                        "WARNING: The following columns were missing in the "
                        "dataframe, and were therefore dropped:"
                    ),
                    dropped_columns,
                    file=sys.stderr,
                )
        elif len(self.selected_columns_) == 0:
            raise KeyError("No columns left after feature selection!")
        elif len(columns) == X.shape[1]:
            return X

        if X[columns].isna().any().any():
            na_cols = X[columns].columns[X[columns].notna().any()]
            print(f"Dropping na columns {na_cols}!")
        return X[columns].dropna(axis=1)


class TPM(PassthroughTransformer):
    """Compute transcript per million (TPM) from raw counts."""

    def __init__(
        self,
        size_annotation_tsv="../input/gene_length_gencode.v36.tsv",
        aggregate: Optional[str] = "gene",
        **kwargs,
    ):
        """
        Args:
            size_annotation_tsv: Path to TSV file with one transcript per row,
                and corresponding gene name and length along the columns.
            aggregate: If not None, sum transcripts lengths by this column.
        """
        super().__init__(**kwargs)
        self.size_annotation_tsv = size_annotation_tsv
        self.aggregate = aggregate

    def fit(self, X: DataFrame, y=None):
        """Load gene size lengths."""
        super().fit(X, y)

        transcript_size = pd.read_csv(self.size_annotation_tsv, sep="\t", index_col=0)
        if self.aggregate in transcript_size.columns:
            self.transcript_size_ = (
                transcript_size.groupby(self.aggregate).sum().squeeze()
            )
        elif self.aggregate is None:
            self.transcript_size_ = transcript_size["length (bp)"]
        else:
            raise ValueError(f"Unknown aggregation column {self.aggregate}.")

        # Check that we know the size of all the features.
        X_raw = X.drop(columns=self.passthrough_)
        missing_features = set(X_raw.columns) - set(self.transcript_size_.index)
        assert (
            missing_features == set()
        ), f"Size mismatch between input and annotation: {missing_features}"
        assert len(set(X_raw.columns)) == len(
            X_raw.columns
        ), "Error: multiple columns with the same name."
        self.transcript_size_ = self.transcript_size_.loc[X_raw.columns]
        return self

    def transform(self, X: DataFrame, y=None):
        """Compute transcript per million."""
        X_counts = X.drop(columns=self.passthrough_)
        normalised_counts = X_counts.divide(self.transcript_size_)
        tpm = normalised_counts.divide(normalised_counts.sum(axis=1), axis=0) * 10**6
        return tpm[sorted(tpm.columns)].join(X[self.passthrough_]).copy()
