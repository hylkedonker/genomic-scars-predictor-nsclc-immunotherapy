from distutils.file_util import copy_file
import gzip
from pathlib import Path
from shutil import copyfileobj
from tempfile import TemporaryDirectory
from typing import Optional

from SigProfilerMatrixGenerator.scripts import (
    CNVMatrixGenerator as scna,
    SigProfilerMatrixGeneratorFunc as matGen,
)
from pandas import DataFrame, Series, read_csv

from src.feature_extraction import get_feature_names


def extract_copy_number_spectra(input_cnv: Path) -> DataFrame:
    """Classify copy number variants by zygosity, segment lenth, and copy number.

    input_cnv: Path to a tab-separated copy number variant file in a PURPLE compatible
        format.

    Returns:
        Copy number segments bucketised into 48 features according to:
        zygosity x length x copy number
    """
    with TemporaryDirectory() as tmp_dir:
        target_dir = Path(tmp_dir) / input_cnv.stem
        scna.generateCNVMatrix(
            file_type="PURPLE",
            input_file=input_cnv,
            project="cnv",
            output_path=str(target_dir) + "/",
        )
        purple_matrix_name = target_dir / "cnv/PURPLE.CNV.matrix.tsv"
        df_purple = read_csv(purple_matrix_name, sep="\t", index_col=0).transpose()

    return df_purple


def extract_mutation_spectra(
    input_vcf: Path, target_folder: Optional[Path] = None, exome: bool = True
) -> dict[str, Series]:
    """
    Classify variants according to mutation type and pool per sample and type.

    Uses SigProfilerMatrixGenerator [1] with GRCh37 to extract the following
    classes:
    - 96 single base substitutions,
    - 78 doublet base substitutions,
    - 83 indels.

    Args:
        input_vcf: Variants to construct spectrum for (a VCF file).
        target_folder: Location to store SBS, DBS, and indel classes.

    Returns: Dictionary of series with number of mutations per mutation type,
        where the keys are the classes: `single_base_substitutions`,
        `doublet_base_substitutions`, and `indel`.

    [1]: Bergstrom EN, Huang MN, Mahto U, Barnes M, Stratton MR, Rozen SG, and
        Alexandrov LB (2019) SigProfilerMatrixGenerator: a tool for visualizing
        and exploring patterns of small mutational events. BMC Genomics 20,
        Article number: 685.
    """
    # Provision temporary folder to run SigProfilerMatrixGenerator.
    with TemporaryDirectory() as tmp_dir:
        # Decompress VCF file for SigProfilerMatrixGenerator.
        if input_vcf.name.endswith("gz"):
            target_vcf = Path(tmp_dir) / input_vcf.name.removesuffix(".gz")
            with gzip.open(input_vcf, "rb") as fo_gz:
                with open(target_vcf, "wb") as fo_out:
                    copyfileobj(fo_gz, fo_out)
        else:
            target_vcf = Path(tmp_dir) / input_vcf.name
            copy_file(str(input_vcf), str(target_vcf))

        # Unique project name.
        name = "mutation-class"
        matrices = matGen.SigProfilerMatrixGeneratorFunc(
            project=name,
            genome="GRCh37",
            vcfFiles=tmp_dir,
            plot=False,
            exome=exome,
            seqInfo=False,
        )

        if target_folder is not None:
            target_folder.mkdir(exist_ok=True, parents=True)

            suffix = "all"
            if exome:
                suffix = "exome"

            # Copy files to target directory.
            sbs_file = Path(tmp_dir) / "output" / "SBS" / f"{name}.SBS96.{suffix}"
            dbs_file = Path(tmp_dir) / "output" / "DBS" / f"{name}.DBS78.{suffix}"
            indel_file = Path(tmp_dir) / "output" / "ID" / f"{name}.ID83.{suffix}"
            copy_file(
                str(sbs_file),
                str(target_folder / sbs_file.name.replace(suffix, "tsv")),
            )
            copy_file(
                str(dbs_file),
                str(target_folder / dbs_file.name.replace(suffix, "tsv")),
            )
            copy_file(
                str(indel_file),
                str(target_folder / indel_file.name.replace(suffix, "tsv")),
            )

    index = input_vcf.name.split(".")[0]

    sbs96_columns = get_feature_names("single_base_substitutions", signatures=False)
    db78_columns = get_feature_names("doublet_base_substitutions", signatures=False)
    indel_columns = get_feature_names("indel", signatures=False)
    X_SBS96 = DataFrame(0, columns=sbs96_columns, index=[index])
    X_DB78 = DataFrame(0, columns=db78_columns, index=[index])
    X_ID83 = DataFrame(0, columns=indel_columns, index=[index])
    if "96" in matrices:
        X_SBS96 = matrices["96"].transpose()
    if "DINUC" in matrices:
        X_DB78 = matrices["DINUC"].transpose()
    if "ID" in matrices:
        X_ID83 = matrices["ID"].transpose()

    return {
        "single_base_substitutions": X_SBS96,
        "doublet_base_substitutions": X_DB78,
        "indel": X_ID83,
    }
