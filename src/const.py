# Significant mutated genes (10) in squamous cell lung cancer, according to TCGA [1].
#
# [1]: Cancer Genome Atlas Research Network. "Comprehensive genomic characterization of
# squamous cell lung cancers." Nature 489.7417 (2012): 519.
TCGA_GENES_SQUAMOUS_LUNG_CANCER = {
    "TP53",
    "CDKN2A",
    "PTEN",
    "PIK3CA",
    "KEAP1",
    "MLL2",
    "HLA-A",
    "NFE2L2",
    "NOTCH1",
    "RB1",
}
# Significant mutated genes (18) in adenocarcinoma lung cancer, according to TCGA [2].
#
# [2]: Cancer Genome Atlas Research Network. "Comprehensive molecular profiling of lung adenocarcinoma." Nature 511.7511 (2014): 543.
TCGA_GENES_ADENO_LUNG_CANCER = {
    "KRAS",
    "TP53",
    "STK11",
    "EGFR",
    "RBM10",
    "KEAP1",
    "CDKN2A",
    "BRAF",
    "U2AF1",
    "SMARCA4",
    "NF1",
    "MET",
    "ARID1A",
    "SETD2",
    "RB1",
    "PIK3CA",
    "MGA",
    "RIT1",
}

# Genes (23) significant in adeno and/or squamous cell carcinoma.
TCGA_GENES_LUNG_CANCER = sorted(
    TCGA_GENES_SQUAMOUS_LUNG_CANCER.union(TCGA_GENES_ADENO_LUNG_CANCER)
)
