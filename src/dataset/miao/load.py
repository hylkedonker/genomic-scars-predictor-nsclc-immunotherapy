"""Load data from external validation set Ref. [1].

[1]: Miao et al., Nat. Gen. 50, 1271 ('18).
"""
import os
from pathlib import Path
import re

from numpy import nan
from pandas import DataFrame, concat, read_csv, read_excel, to_timedelta

from src.dataset.utils import durable_benefit
from src.feature_extraction import get_feature_names
from src.utils import pyrimidine_single_base_substitution


MIAO_DIR = Path(os.path.dirname(__file__))


def _impose_exclusion_criteria(X_clinic):
    """Impose dataset exclusion criteria on Miao dataset."""
    print("Excluding:")

    is_lung = X_clinic.cancer_type == "Lung"
    print("- Non lung cancer (n = {})".format(X_clinic[~is_lung].shape[0]))
    X_clinic = X_clinic[is_lung]

    # Exclude wrong lung cancer subtype.
    is_sclc = X_clinic["histology"] == "small cell lung cancer"
    print("- small-cell lung cancer (n = {})".format(X_clinic[is_sclc].shape[0]))
    X_clinic = X_clinic[~is_sclc]

    print("Total: n = {}".format(X_clinic.shape[0]))
    return X_clinic


def _extract_spectrum(X_sbs96):
    """Count number of single base substitution transitions per Miao patient."""
    index = X_sbs96.pair_id.unique()
    X_spectrum = DataFrame(
        0,
        index=index,
        columns=get_feature_names("single_base_substitutions", signatures=False),
    )
    for patient_id in index:
        is_ptid = X_sbs96.pair_id == patient_id
        x_counts = X_sbs96.loc[is_ptid, "sbs-96"].value_counts()
        X_spectrum.loc[patient_id, x_counts.index] = x_counts

    return X_spectrum


def annotate_single_base_substitution(X_variants):
    """Extract single base substution 96 features from Miao."""
    is_snv = X_variants.Variant_Type == "SNP"
    X_variants = X_variants[is_snv].copy()
    ref = X_variants.Reference_Allele
    alt = X_variants.Tumor_Seq_Allele2

    # Verify that we picked up alterations.
    assert (alt != ref).all()

    # Build triplet.
    left = X_variants.ref_context.map(lambda x: x[9])
    right = X_variants.ref_context.map(lambda x: x[11])
    triplet = left + ref + right

    sbs = DataFrame({"triplet_ref": triplet.astype(str), "alt": alt.astype(str)})
    X_variants["sbs-96"] = sbs.apply(
        lambda x: pyrimidine_single_base_substitution(
            x["triplet_ref"].upper(), x["alt"].upper()
        ),
        axis=1,
    )
    return X_variants


def load_variants():
    """Load variants from Supplementary Table 5, Ref. [1]."""
    X_variants = read_csv(MIAO_DIR / "somatic_mutations.txt", sep="\t")

    # Unfortunately, this dataset has no doublet base substitutions.
    is_ref_doublet = X_variants.Reference_Allele.apply(len) == 2
    is_alt_doublet = X_variants.Tumor_Seq_Allele2.apply(len) == 2
    assert X_variants[is_ref_doublet & is_alt_doublet].size == 0

    # Only keep non-synonymous variants.
    X_variants = X_variants[X_variants.Protein_Change.notna()]
    X_variants = X_variants[X_variants.Variant_Classification != "Silent"]

    individual_sbs96_variants = annotate_single_base_substitution(X_variants)
    X_sbs96 = _extract_spectrum(individual_sbs96_variants)
    # Count number of record per `pair_id`. We take an arbitrariy column
    # ('Start_position') without `nan` to extract counts.
    x_tmb = X_variants.groupby("pair_id")["Start_position"].count()
    x_tmb.name = "tmb"

    # Verify that the single base substitutions coincide with the number of
    # SNV/SNP's.
    n_sbs_mutations = individual_sbs96_variants.groupby("pair_id")["sbs-96"].count()
    is_snp = X_variants.Variant_Type == "SNP"
    # We take an arbitrariy column ('Start_position') without `nan` to extract counts.
    n_snp_mutations = X_variants[is_snp].groupby("pair_id")["Start_position"].count()
    assert all(n_sbs_mutations == n_snp_mutations)

    # Number of SNV's is a strict subset of non-synonymous variants.
    assert all(n_sbs_mutations <= x_tmb)
    assert not all(n_sbs_mutations == x_tmb)  # But there are some none SNV's.
    return concat([X_sbs96, x_tmb], axis="columns")


def load_metadata():
    """Polish data from Supplementary Table 1 + 2, Ref [1]."""
    X_clinic = read_excel(MIAO_DIR / "patient_characteristics.xlsx", index_col=0)
    X_clinic = X_clinic.rename(columns={"age_start_io": "age", "sex": "gender"})

    X_clinic.smoker = X_clinic.smoker.str.lower().replace(
        {"former ": "former", "yes": "current"}
    )

    X_clinic["gender"] = X_clinic["gender"].str.lower()
    X_clinic["histology"] = X_clinic["histology"].str.lower()
    X_clinic["treatment"] = (
        X_clinic["drug_type"]
        .str.lower()
        .replace({"anti-pd-1/anti-pd-l1": "αPD-1/αPD-L1"})
    )

    X_clinic["pfs_days"] = to_timedelta(X_clinic["pfs_days"], unit="days")
    X_clinic["pfs_event"] = 1 - X_clinic["pfs_censor"]

    X_clinic["os_days"] = to_timedelta(X_clinic["os_days"], unit="days")
    X_clinic["os_event"] = 1 - X_clinic["os_censor"]

    X_clinic["durable_benefit"] = durable_benefit(X_clinic[["pfs_days", "pfs_event"]])
    X_clinic["stage"] = nan
    X_clinic["prior_therapy"] = nan
    X_clinic["pre_treatment"] = nan
    X_clinic["tumor_purity"] = nan
    X_clinic["tissue"] = "formalin-fixed paraffin-embedded"

    X_samples = read_csv(
        MIAO_DIR / "supplementary_table1.csv", index_col=0, skiprows=28
    )
    X_samples = X_samples[X_samples["included_meta-analysis"] == 1]

    # The index in `X_samples` is an abbreviation of the index in `X_clinic`.
    # Try to match indices in X_clinic with X_samples by looking for abbreviations.
    left_index, right_index = [], []
    for i in X_clinic.index:
        for j in X_samples.index:
            pattern = re.escape(j) + "[_\-]"
            # if i.startswith(j):
            if re.search(pattern, i, flags=re.IGNORECASE) is not None:
                left_index.append(i)
                right_index.append(j)
                break
    # sample_index = [i for j in X_samples.index for i in X_clinic.index if i.startswith(j)]
    assert len(set(left_index)) == len(set(right_index))

    X_clinic.loc[left_index, "tumor_purity"] = X_samples.loc[
        right_index, "purity"
    ].to_numpy()
    # Base on Supplementary Table 1 in the paper:
    # Whole-exome sequencing and clinical interpretation of formalin-fixed,
    # paraffin-embedded tumor samples to guide precision cancer medicine
    # X_clinic["region covered (mb)"] = 32.95
    X_clinic.loc[left_index, "region covered (mb)"] = (
        X_samples.loc[right_index, "somatic_mutation_covered_bases_capture"].to_numpy()
        / 1e6
    )
    return _impose_exclusion_criteria(X_clinic)


def load():
    """Load dataset with mutation spectrum."""
    X_clinic = load_metadata()
    X_variants = load_variants()

    # Select variants of patients in the metadata spreadsheet.
    is_lung = X_variants.index.isin(X_clinic.index)
    X_variants = X_variants[is_lung]

    # Verify that the dataset is complete.
    assert set(X_clinic.index.unique()) - set(X_variants.index.unique()) == set()
    assert set(X_variants.index.unique()) - set(X_clinic.index.unique()) == set()

    X_clinic = X_clinic[
        [
            "age",
            "gender",
            "histology",
            "prior_therapy",
            "pre_treatment",
            "stage",
            "smoker",
            "tumor_purity",
            "treatment",
            "tissue",
            "durable_benefit",
            "pfs_days",
            "pfs_event",
            "os_days",
            "os_event",
            "RECIST",
            "region covered (mb)",
        ]
    ]
    return concat([X_clinic, X_variants], axis="columns")
