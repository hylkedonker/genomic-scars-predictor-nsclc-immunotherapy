"""Data from Miao et al., Nat. Gen. 50, 1271 ('18).

Genomic correlates of response to immune checkpoint blockade in
microsatellite-stable solid tumors
"""

from .load import load as load_miao
