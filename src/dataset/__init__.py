from pathlib import Path
import os


DATASET_DIR = Path(os.path.dirname(__file__))
HARTWIG_DIR = DATASET_DIR / "hartwig"
CUPPENS_DIR = DATASET_DIR / "cuppens"
ARTIFACT_DIR = DATASET_DIR / ".." / ".." / "artifacts"
