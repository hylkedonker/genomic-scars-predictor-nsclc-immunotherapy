from datetime import timedelta

from pandas import DataFrame, Series
from numpy import nan


def durable_benefit(pfs: DataFrame) -> Series:
    """Compute durable benefit: no progressive disease within half a year."""
    y = Series(nan, index=pfs.index)
    pfs_halfyear = pfs["pfs_days"] >= timedelta(weeks=52 // 2)
    y.loc[pfs_halfyear] = 1
    is_observed = pfs["pfs_event"] == 1
    y.loc[is_observed & (~pfs_halfyear)] = 0
    return y
