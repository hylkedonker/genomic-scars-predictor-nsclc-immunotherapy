import os
import subprocess
import sys
from tempfile import mkdtemp

from pandas import DataFrame, read_csv


def run_r_script(
    script_file: os.PathLike, input_data: DataFrame, *script_flags
) -> DataFrame:
    """Run R-script by storing script input and output in temp files."""
    tmp_dir = mkdtemp()
    input_file = os.path.join(tmp_dir, "input.tsv")
    output_file = os.path.join(tmp_dir, "output.csv")
    input_data.to_csv(input_file, sep="\t", index=False)

    command = [
        "Rscript",
        "--quiet",
        "--vanilla",
        script_file,
        input_file,
        output_file,
        *script_flags,
    ]
    # Assume first argument is input csv, and second argument is output csv.
    try:
        output = subprocess.run(
            command,
            capture_output=True,
            check=True,
        )
    except subprocess.CalledProcessError as e:
        raise RuntimeError(
            "command '{}' return with error (code {}): {}".format(
                e.cmd, e.returncode, e.output
            )
        )
    print(output.stdout.decode("ascii", "ignore"), file=sys.stdout)
    print(output.stderr.decode("ascii", "ignore"), file=sys.stderr)
    return read_csv(output_file)


# class DESeq2(BaseFilter):
#     def __init__(
#         self,
#         padjusted: float = 0.1,
#         num_features: int = 50,
#         to_tpm: bool = False,
#         covariates: list = ["batch"],
#         passthrough: list = [
#             "pre_treatment",
#             "tumor_purity",
#         ],
#         passthrough_housekeeping: Optional[str] = "hartwig-budczies-cho",
#         **kwargs,
#     ):
#         """
#         Perform differential gene expression using DESeq2.

#         Args:
#             covariates: Non-expression columns passed to DESeq2 script (to be
#                 used, e.g., in the design matrix, if specified).
#                 !! N.B.: The R-script is responsible for using these    !!
#                 !! covariates. This estimator does not do this for you. !!

#             passthrough: Columns not part of the feature selection, but are
#                 directly passed through: Neither expression data, nor
#                 covariates.

#             passthrough_housekeeping: Genes not to consider for differential
#                 expression, but are directly passedthrough (for downstream
#                 housekeeping normalisation).
#         """
#         super().__init__(passthrough=passthrough, **kwargs)

#         self.passthrough_housekeeping = passthrough_housekeeping
#         self.padjusted = padjusted
#         self.covariates = covariates
#         self.num_features = num_features
#         self.to_tpm = to_tpm

#     def fit(self, X: pd.DataFrame, y=None):
#         """Run DESeq2 R-script without housekeeping genes and with covariates."""
#         super().fit(X, y)
#         if self.to_tpm and self.passthrough_housekeeping is not None:
#             raise ValueError(
#                 "Cannot compute TPM when passing through housekeeping genes."
#             )

#         self.house_keeping_ = []
#         if self.passthrough_housekeeping:
#             self.house_keeping_ = sorted(
#                 set(X.columns).intersection(
#                     HOUSEKEEPING_GENES[self.passthrough_housekeeping]
#                 )
#             )

#         # Check consistency of covariates and passthrough columns.
#         assert all(X[self.covariates].var() > 0)
#         assert set(self.passthrough).intersection(self.covariates) == set()

#         self.rscript_ = "Rscripts/DESeq2.R"
#         if len(self.covariates) > 0:
#             self.rscript_ = "Rscripts/DESeq2_batch.R"

#         X_counts = X.drop(columns=self.passthrough_ + self.house_keeping_)
#         if self.to_tpm:
#             self.transform_ = TPM(passthrough=[]).fit(X_counts)

#         Xy = X_counts.join(y, how="inner").reset_index()
#         diff_expr_result = run_r_script(
#             self.rscript_,
#             Xy,
#             str(self.padjusted),
#             str(self.num_features),
#         )
#         # First column contains differentially expressed genes.
#         diff_expr_result.set_index(diff_expr_result.columns[0], inplace=True)
#         self.selected_columns_ = diff_expr_result.index.to_list()
#         self.pvals_ = diff_expr_result["pvalue"]
#         self.log2FC_ = diff_expr_result["log2FoldChange"]
#         self.scores_ = {
#             "gene": pd.Series(self.selected_columns_),
#             "pvalue": pd.Series(self.pvals_),
#             "log2FC": pd.Series(self.log2FC_),
#         }
#         return self

#     def transform(self, X, y=None):
#         """Convert to TPM (if necessary), and add passthrough columns."""
#         X_genes = X.drop(
#             columns=self.passthrough_ + self.house_keeping_ + self.covariates
#         )
#         if self.to_tpm:
#             X_genes = self.transform_.transform(X_genes)
#         # Put back housekeeping genes and covariates after DESeq2 feature
#         # selection.
#         put_back = self.passthrough_ + self.house_keeping_ + self.covariates
#         return X_genes[self.selected_columns_].join(X[put_back]).copy()
