from numpy import log, sqrt
from scipy.stats import norm
from statkit.types import Estimate


def regression_coefficient_test(
    coef_1: Estimate, coef_2: Estimate, alternative="two-sided"
) -> float:
    f"""Equal regression coefficient hypothesis test.

    H0: Both coefficients are sampled from the same distribution.

    Args:
        coef_1, coef_2: Coefficient estimate (key="point") and 95 % confidence interval
        (key="upper" and key="lower") of both groups.
        alternative: When 'two-sided', test for both larger and smaller alternative
            distribution.

    [1]: Paternostor et al., Using the Correct Statistical Test for Equality of
    Regression Coefficients, Criminology 36, p. 859 (1998).

    Returns:
        the z-statistic and the p-value.
    """
    sigma_1 = (coef_1.upper - coef_1.lower) / (2 * 1.96)
    sigma_2 = (coef_2.upper - coef_2.lower) / (2 * 1.96)

    Z = abs(coef_1.point - coef_2.point) / sqrt(sigma_1**2 + sigma_2**2)

    if alternative == "two-sided":
        p = (1 - norm.cdf(Z)) * 2

    return Z, p


def hazard_ratio_test(
    hazard_ratio_1: Estimate, hazard_ratio_2: Estimate, alternative="two-sided"
):
    """Test if two hazard ratios are significantly different.

    H0: Both hazard ratios are sampled from the same underlying distrubution.

    Args:
        hazard_ratio_1, hazard_ratio_2:
        alternative: When 'two-sided', test for both larger and smaller alternative
            distribution.

    """
    # Use the fact that hazard ratio is defined as:
    # h = exp(-w)
    coef_1 = Estimate(
        point=-log(hazard_ratio_1.point),
        lower=-log(hazard_ratio_1.lower),
        upper=-log(hazard_ratio_1.upper),
    )
    coef_2 = Estimate(
        point=-log(hazard_ratio_2.point),
        lower=-log(hazard_ratio_2.lower),
        upper=-log(hazard_ratio_2.upper),
    )

    return regression_coefficient_test(coef_1, coef_2, alternative=alternative)
