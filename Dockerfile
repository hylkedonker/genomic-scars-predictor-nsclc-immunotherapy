FROM registry.gitlab.com/hylkedonker/genomic-scars-predictor-nsclc-immunotherapy:GRCh37
ARG DEBIAN_FRONTEND=noninteractive

COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . /app/
WORKDIR /app/