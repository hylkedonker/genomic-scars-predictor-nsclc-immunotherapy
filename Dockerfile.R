###########
# Stage 1 #
###########
FROM r-base AS R_BUILD
ENV DEBIAN_FRONTEND=noninteractive

# r-base-core \
RUN apt update  \
    && apt install --assume-yes \
    python3-pip \
    python3-dev \
    cmake
RUN apt install --assume-yes \
    r-cran-dplyr \
    r-bioc-deseq2 \
    r-cran-biocmanager \
    r-cran-devtools
RUN apt update --allow-releaseinfo-change \
    && apt install --assume-yes \
    libssl-dev \
    libcurl4-openssl-dev \
    libxml2-dev

RUN apt update
RUN apt install -y -V ca-certificates lsb-release wget
RUN wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
RUN apt install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
RUN apt update
RUN apt install -y -V \
    libarrow-dev


RUN R -e "install.packages('Ball',dependencies=TRUE)"
RUN R -e "BiocManager::install('apeglm', dependencies=TRUE)"
RUN R -e "BiocManager::install('WGCNA', dependencies=TRUE)"
RUN R -e 'devtools::install_github("zhangyuqing/sva-devel", force=TRUE)'
RUN R -e "install.packages('fastclime',dependencies=TRUE)"
RUN R -e "install.packages('flare',dependencies=TRUE)"
# Re-intall R-lang so that we get the latest version for arrow.
RUN R -e "install.packages('rlang',dependencies=TRUE)"

RUN R -e "install.packages('arrow',dependencies=TRUE)"
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN mkdir /usr/src/app/
WORKDIR /usr/src/app/
RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels cmake==3.18.4
# RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt

###########
# Stage 2 #
###########
FROM r-base

RUN apt update \
    && apt install --assume-yes --no-install-recommends \
    python3-pip \
    python3-openpyxl \
    python3-xlrd \
    r-cran-dplyr \
    r-bioc-deseq2 \
    r-cran-biocmanager \
    r-cran-devtools

# Copy previously build R packages.
COPY --from=R_BUILD /usr/local/lib/R/site-library/ /usr/local/lib/R/site-library/
COPY --from=R_BUILD /usr/src/app/wheels /wheels
# COPY --from=R_BUILD /usr/src/app/requirements.txt .

RUN pip install --no-cache /wheels/*

# RUN apt install r-cran-rlang

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1
# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1


WORKDIR /app/
# Creates a non-root user with an explicit UID and adds permission to access the /app folder
# For more info, please refer to https://aka.ms/vscode-docker-python-configure-containers

COPY . /app/
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
# USER appuser

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["python3", "run.py"]
