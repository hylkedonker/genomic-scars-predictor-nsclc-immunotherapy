from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase, skip
import warnings

from pandas import DataFrame, isna

from src.dataset import ARTIFACT_DIR, HARTWIG_DIR
from src.dataset.build import build_filtered_vcfs
from src.feature_extraction.step1.filter_vcf import (
    filter_synonymous_variants,
    get_annotation_value,
    is_non_synonymous,
)

TEST_RESOURCES = Path(__file__).parent.resolve() / "resources"
TEST_VCF_GRCH37 = TEST_RESOURCES / "GRCh37"


def _count_lines(filename: Path, ignore_prefix="#") -> int:
    with open(str(filename)) as fo:
        lines = fo.readlines()
        if ignore_prefix is not None:
            return len([l for l in lines if not l.startswith(ignore_prefix)])
        return len(lines)


class TestVariantFilter(TestCase):
    synonymous_annotation = {
        "Allele": "A",
        "Annotation": "splice_region_variant&synonymous_variant",
        "Annotation_Impact": "LOW",
        "Gene_Name": "DLEU1",
        "Gene_ID": "ENSG00000176124",
        "Feature_Type": "transcript",
        "Feature_ID": "ENST00000378180",
        "Transcript_BioType": "protein_coding",
        "Rank": "2/2",
        "HGVS.c": "c.123G>A",
        "HGVS.p": "p.Val41Val",
        "cDNA.pos / cDNA.length": "389/981",
        "CDS.pos / CDS.length": "123/237",
        "AA.pos / AA.length": "41/78",
        "Distance": "",
        "ERRORS / WARNINGS / INFO": "",
    }
    empty_annotation = {
        "Allele": "",
        "Annotation": "",
        "Annotation_Impact": "",
        "Gene_Name": "",
        "Gene_ID": "",
        "Feature_Type": "",
        "Feature_ID": "",
        "Transcript_BioType": "",
        "Rank": "",
        "HGVS.c": "",
        "HGVS.p": "",
        "cDNA.pos / cDNA.length": "",
        "CDS.pos / CDS.length": "",
        "AA.pos / AA.length": "",
        "Distance": "",
        "ERRORS / WARNINGS / INFO": "",
    }

    def test_get_gene(self):
        """Test extraction of gene from multiple annotations."""
        no_gene = get_annotation_value(annotations=(self.empty_annotation,), key=3)
        self.assertTrue(isna(no_gene))

        synonymous_gene = get_annotation_value(
            annotations=(self.empty_annotation, self.synonymous_annotation), key=3
        )
        self.assertEqual(synonymous_gene, "DLEU1")

    def test_is_non_synonymous(self):
        """Test identification of non-synonymous variants by annotation."""

        self.assertFalse(is_non_synonymous(self.synonymous_annotation))

    def test_soft_filter(self):
        """Test that correct variants are filtered but not removed."""
        sample1_vcf = TEST_VCF_GRCH37 / "sample1.vcf"
        with TemporaryDirectory() as tmp_dir:
            tmp_file = Path(tmp_dir) / "sample1.filtered.vcf"
            filter_synonymous_variants(
                input_vcf=sample1_vcf,
                filtered_vcf=tmp_file,
                soft=True,
            )

            self.assertEqual(_count_lines(sample1_vcf), _count_lines(tmp_file))

            with open(tmp_file) as fo:
                variants = [line for line in fo.readlines() if not line.startswith("#")]
                # Select filtered variants.
                filtered_variants = filter(lambda x: "IS_SYNON" in x, variants)

                # Combine chromosome and position, for easy comparison.
                positions = {":".join(var.split("\t")[:2]) for var in filtered_variants}

                # The following variants are synonymous.
                self.assertEqual(
                    set(positions),
                    {
                        "chr1:11181327",
                        "chr4:1803556",
                        "chrX:149083064",
                        "chrX:149162806",
                    },
                )

    def test_hard_filter(self):
        """Test that correct variants are filtered and removed."""
        sample1_vcf = TEST_VCF_GRCH37 / "sample1.vcf"
        with TemporaryDirectory() as tmp_dir:
            tmp_file = Path(tmp_dir) / "sample1.filtered.vcf"
            filter_synonymous_variants(
                input_vcf=sample1_vcf,
                filtered_vcf=tmp_file,
                soft=True,
            )

            self.assertEqual(_count_lines(sample1_vcf), _count_lines(tmp_file))

            with open(tmp_file) as fo:
                variants = [line for line in fo.readlines() if not line.startswith("#")]
                # only select unfiltered variants.
                unfiltered_variants = filter(lambda x: "IS_SYNON" not in x, variants)

                # Combine chromosome and position, for easy comparison.
                positions = {
                    ":".join(var.split("\t")[:2]) for var in unfiltered_variants
                }

                # The following variants are non-synonymous.
                self.assertEqual(
                    set(positions),
                    {
                        "chr1:115256529",
                        "chr5:112128191",
                        "chr4:1807803",
                        "chr1:149785104",
                        "chr11:209584",
                        "chr9:133759489",
                    },
                )


class TestFilterHartwig(TestCase):
    @skip("This public repository doesn't contain the raw data.")
    def test_filtered_vcf(self):
        """Test that only protein chaning variants are retained."""
        df = DataFrame(
            {
                "vcf": [
                    HARTWIG_DIR / "vcf" / "CPCT02010272T.purple.somatic.vcf.gz",
                    ARTIFACT_DIR / "vcf_RNA/CPCT02020485T.rna.vcf",
                ]
            },
            index=["CPCT02010272T", "CPCT02020485T"],
        )
        with TemporaryDirectory() as tmpdir:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                new_df = build_filtered_vcfs(
                    df,
                    artifact_dir=Path(tmpdir),
                    filter_function=filter_synonymous_variants,
                    verbose=False,
                )
            with open(new_df.loc["CPCT02010272T", "vcf"]) as fo:

                variants = [line for line in fo.readlines() if not line.startswith("#")]
                # Combine chromosome and position, for easy comparison.
                positions = {":".join(var.split("\t")[:2]) for var in variants}
                self.assertTrue("13:50678841" not in positions)

            with open(new_df.loc["CPCT02020485T", "vcf"]) as fo:
                variants = [line for line in fo.readlines() if not line.startswith("#")]
                # Combine chromosome and position, for easy comparison.
                positions = {":".join(var.split("\t")[:2]) for var in variants}
                self.assertTrue("22:37851338" not in positions)
