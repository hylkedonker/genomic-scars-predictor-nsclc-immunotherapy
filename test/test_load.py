from unittest import TestCase, skip

from pandas import Timedelta

from src.dataset.hartwig import load_hartwig
from src.dataset.hartwig.load import load_hartwig_metadata
from src.dataset.miao.load import load_metadata as load_miao_metadata


@skip("This public repository doesn't contain the raw data.")
class TestMetadata(TestCase):
    def test_sample_size(self):
        """Test sample size coincides with that given in the paper."""
        X_hartwig = load_hartwig_metadata()
        self.assertEqual(len(X_hartwig), 101)
        X_miao = load_miao_metadata()
        self.assertEqual(len(X_miao), 56)

    def test_hartwig_survival_data(self):
        """Test for causility in Hartwig survival data."""
        X_clinic = load_hartwig_metadata()
        self.assertTrue(all(X_clinic["pfs_days"] <= X_clinic["os_days"]))

        self.assertEqual(X_clinic.loc["HRTW_CPCT02020964T", "os_event"], 1)
        self.assertEqual(X_clinic.loc["HRTW_CPCT02020964T", "os_days"], 47)

        # This person, there was a mistake in the survival data.
        self.assertEqual(X_clinic.loc["HRTW_CPCT02011069T", "os_event"], 0)
        self.assertTrue(X_clinic.loc["HRTW_CPCT02011069T", "os_days"] > 0)

    def test_load(self):
        """Test the artifact-cached load function."""
        data = load_hartwig()
        # We do not allow 0 < pfs_days < 1
        illegal_dt = (Timedelta("0 days") < data["pfs_days"]) & (
            data["pfs_days"] < Timedelta("1 day")
        )
        self.assertFalse(any(illegal_dt))

    def test_miao_survival_data(self):
        """Test for causility in Miao et al. survival data."""
        X_clinic = load_miao_metadata()
        self.assertTrue(all(X_clinic["pfs_days"] <= X_clinic["os_days"]))
