from unittest import TestCase

from pandas import read_csv

from src.feature_extraction import COSMIC_MUTATIONAL_SIGNATURES
from src.utils import (
    pyrimidine_base_triplet,
    pyrimidine_doublet_base_substitution,
    reverse_complement,
    pyrimidine_single_base_substitution,
    quantile_threshold,
)


class TestUtils(TestCase):
    def test_reverse_complement(self):
        """Test function with ground truth."""
        self.assertEqual(reverse_complement("AGC"), "GCT")
        self.assertEqual(reverse_complement("AAC"), "GTT")
        self.assertEqual(reverse_complement("ACG"), "CGT")
        self.assertEqual(reverse_complement("AAG"), "CTT")
        self.assertEqual(reverse_complement("CGT"), "ACG")
        self.assertEqual(reverse_complement("CTT"), "AAG")

    def test_pyrimine_convention(self):
        """Test convention with answers from [1].

        [1]: Bergstrom et al. BMC Genomics, 20:685 ('19).
        """
        self.assertEqual(pyrimidine_base_triplet("AGC"), "GCT")
        self.assertEqual(pyrimidine_base_triplet("CGT"), "ACG")
        self.assertEqual(pyrimidine_base_triplet("ACG"), "ACG")
        self.assertEqual(pyrimidine_base_triplet("AAC"), "GTT")
        self.assertEqual(pyrimidine_base_triplet("CTT"), "CTT")
        self.assertEqual(pyrimidine_base_triplet("ACG"), "ACG")
        self.assertEqual(pyrimidine_base_triplet("AAG"), "CTT")

    def test_to_sbs(self):
        """Turn triplet with alternate mutation to triplet notation."""
        self.assertEqual(pyrimidine_single_base_substitution("AGC", "A"), "G[C>T]T")
        self.assertEqual(pyrimidine_single_base_substitution("GCT", "T"), "G[C>T]T")
        self.assertEqual(pyrimidine_single_base_substitution("ACG", "A"), "A[C>A]G")
        self.assertEqual(pyrimidine_single_base_substitution("CGT", "T"), "A[C>A]G")

    def test_quantile_threshold(self):
        """Test that threshold is correctly computed."""
        signatures = read_csv(
            COSMIC_MUTATIONAL_SIGNATURES["single_base_substitutions"],
            index_col=0,
            sep="\t",
        )
        sbs4 = signatures["SBS4"]

        threshold_04 = quantile_threshold(sbs4, quantile=0.4)
        self.assertTrue(0.4 <= sbs4[sbs4 >= threshold_04].sum() < 0.45)

        threshold_06 = quantile_threshold(sbs4, quantile=0.6)
        self.assertTrue(0.6 <= sbs4[sbs4 >= threshold_06].sum() < 0.65)

        # Test trivial idendities.
        threshold_0 = quantile_threshold(sbs4, quantile=0.0)
        # self.assertTrue()
        self.assertEqual(threshold_0, max(sbs4))

        threshold_1 = quantile_threshold(sbs4, quantile=1.0)
        self.assertEqual(threshold_1, min(sbs4))
