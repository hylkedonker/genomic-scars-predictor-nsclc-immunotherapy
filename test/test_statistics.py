from unittest import TestCase

from numpy.testing import assert_almost_equal
from statkit.types import Estimate

from src.statistics import hazard_ratio_test, regression_coefficient_test


class TestCoefficientTest(TestCase):
    def test_pvalue_rejections(self):
        """Test hypothesis rejection examples from Ref. [1].

        [1]: Paternostor et al., Using the Correct Statistical Test for Equality of
        Regression Coefficients, Criminology 36, p. 859 (1998).
        """

        def as_estimate(mu, sigma):
            return Estimate(point=mu, lower=mu - 1.96 * sigma, upper=mu + 1.96 * sigma)

        # Example p. 863, Ref. [1].
        b_1, b_1_se = 0.404, 0.094
        b_2, b_2_se = 0.221, 0.091
        z, p = regression_coefficient_test(
            as_estimate(b_1, b_1_se), as_estimate(b_2, b_2_se)
        )
        assert_almost_equal(z, 1.4, decimal=1)
        self.assertGreater(p, 0.05)

        # Example p. 863-864, Ref [1].
        b_1, b_1_se = -0.116, 0.072
        b_2, b_2_se = -0.269, 0.071
        z, p = regression_coefficient_test(
            as_estimate(b_1, b_1_se), as_estimate(b_2, b_2_se)
        )
        assert_almost_equal(z, 1.51, decimal=2)
        self.assertGreater(p, 0.05)
        self.assertLess(p, 1)

    def test_two_sided(self):
        """Test that the statistical test is two sided."""
        hr_1 = Estimate(
            0.5681664295581945, lower=0.3251718994288671, upper=0.9927459668067774
        )
        hr_2 = Estimate(0.61, lower=0.34, upper=1.1)

        _, p12_value = hazard_ratio_test(hr_1, hr_2)
        self.assertTrue(p12_value < 1)

        # p-value with arguments reversed should be identicla.
        _, p21_value = hazard_ratio_test(hr_2, hr_1)
        self.assertEqual(p21_value, p12_value)
