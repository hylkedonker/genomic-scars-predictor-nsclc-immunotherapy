from datetime import timedelta
from pathlib import Path
from typing import Tuple

from lifelines import CoxPHFitter, KaplanMeierFitter
from lifelines.plotting import add_at_risk_counts
from matplotlib import pyplot as plt
from numpy import nan
from pandas import DataFrame, concat, isna, to_timedelta
from sklearn.metrics import RocCurveDisplay, roc_auc_score
import statkit
from statkit.non_parametric import bootstrap_score
from statkit.types import Estimate
from statkit.views import format_p_value
import numpy as np
import pandas as pd


def polish_variable_names(dataframe) -> DataFrame:
    """Human readable variable names for in the plots."""
    data = dataframe.copy()

    def _has_substr(x, substr):
        if isna(x):
            return nan
        elif substr in x:
            return "Yes"
        return "No"

    data["Prior radiotherapy"] = data["prior_therapy"].map(
        lambda x: _has_substr(x, "radiotherapy")
    )

    data["Prior chemotherapy"] = data["prior_therapy"].map(
        lambda x: _has_substr(x, "chemotherapy")
    )
    data["Treatment naive"] = data["prior_therapy"].map(
        lambda x: _has_substr(x, "naive")
    )
    data["Durable benefit"] = data.pop("durable_benefit").map({0.0: "No", 1.0: "Yes"})
    return data


def X_y(data):
    X = polish_variable_names(data)
    X["pfs_weeks"] = to_timedelta(X.pop("pfs_days"), unit="days") / timedelta(weeks=1)
    return (
        X.drop(columns="Durable benefit").copy(),
        X["Durable benefit"].replace({"Yes": 1, "No": 0}).copy(),
    )


def stratify_survival(stratum, time, event):
    """Split survival data by stratum label."""
    time_A, time_B = time[stratum], time[~stratum]
    event_A, event_B = event[stratum], event[~stratum]

    return DataFrame(
        {
            "pfs_weeks": concat([time_A, time_B]),
            "pfs_event": concat([event_A, event_B]),
            "stratum": concat([time_A * 0 + 1, time_B * 0]),
        }
    )


def get_hazard_ratio(pfs: DataFrame) -> Tuple[Estimate, float]:
    cph = CoxPHFitter()
    cph.fit(pfs, duration_col="pfs_weeks", event_col="pfs_event")
    hazard_ratio = cph.summary.loc[
        "stratum", ["exp(coef)", "exp(coef) lower 95%", "exp(coef) upper 95%"]
    ]
    p_value = cph.summary.loc["stratum", "p"]
    hr_estimate = Estimate(
        point=hazard_ratio["exp(coef)"],
        lower=hazard_ratio["exp(coef) lower 95%"],
        upper=hazard_ratio["exp(coef) upper 95%"],
    )
    return hr_estimate, p_value


def plot_kaplan_meier(pfs: DataFrame, labels) -> Estimate:
    """Stratify Kaplan-Meier by label."""
    stratum = pfs["stratum"] == 1

    time_A = pfs.loc[stratum, "pfs_weeks"]
    event_A = pfs.loc[stratum, "pfs_event"]
    time_B = pfs.loc[~stratum, "pfs_weeks"]
    event_B = pfs.loc[~stratum, "pfs_event"]

    # Train kaplan-meier.
    km_ndb = KaplanMeierFitter().fit(time_B, event_observed=event_B)
    print("Median survival", labels[0], ":", km_ndb.median_survival_time_)
    km_db = KaplanMeierFitter().fit(time_A, event_observed=event_A)
    print("Median survival", labels[1], ":", km_db.median_survival_time_)

    plt.figure(figsize=(4, 4.5))
    ax = km_db.plot(label=labels[1], show_censors=True, ci_show=False)
    km_ndb.plot(label=labels[0], ax=ax, show_censors=True, ci_show=False)

    # Compute and plot hazard ratio.
    hazard_ratio, p_value = get_hazard_ratio(pfs)
    hr_label = r"$h_r={}$ ({})".format(
        hazard_ratio.latex().replace("$", ""),
        format_p_value(p_value, latex=True, symbol="p"),
    )
    plt.legend(frameon=False, title=hr_label, loc="upper right")
    print("Hazard ratio", hr_label)

    plt.xlabel("Time (weeks)")
    plt.ylabel("Progression free survival (-)")

    add_at_risk_counts(
        km_ndb, km_db, ax=ax, labels=labels, at_risk_count_from_start_of_period=True
    )
    plt.tight_layout()

    return hazard_ratio


def plot_roc_curve(y_true, y_prob, name, threshold_probability=None, ax=None):
    if ax is None:
        f = plt.figure(figsize=(4, 3))
        ax = f.gca()

    # Compute ROC AUC with 95 % CI.
    auc = bootstrap_score(y_true, y_prob, metric=roc_auc_score)
    RocCurveDisplay.from_predictions(y_true, y_prob, name=name, ax=ax)
    ax.lines[-1].set_label("{}\n(AUC = {})".format(name, auc.latex()))
    # ax.plot([0, 1], [0, 1], "--", color="r", linewidth=2, label="Random (AUC: 0.5)")

    if threshold_probability is not None:
        y_pred = (y_prob >= threshold_probability).astype(int)
        sensitivity_score = bootstrap_score(
            y_true, y_pred, metric=statkit.metrics.sensitivity
        )
        specificity_score = bootstrap_score(
            y_true, y_pred, metric=statkit.metrics.specificity
        )
        # Plot classifier sensitivity and specificity.
        specificity_error = [
            [specificity_score.point - specificity_score.lower],
            [specificity_score.upper - specificity_score.point],
        ]
        sensitivity_error = [
            [sensitivity_score.point - sensitivity_score.lower],
            [sensitivity_score.upper - sensitivity_score.point],
        ]
        ax.errorbar(
            [1 - specificity_score.point],
            [sensitivity_score.point],
            xerr=specificity_error,
            yerr=sensitivity_error,
            # label=f"{name} $Pr \geq$ {threshold_probability*100:.1f} %",
        )

    ax.legend(frameon=False)
    ax.set_ylabel("True positive rate")
    ax.set_xlabel("False positive rate")
    return ax


def binarize(y, threshold=(0.5, 0.5)):
    y[np.argwhere(y > threshold[1])] = 1
    y[np.argwhere(y < threshold[0])] = 0
    return y, np.argwhere((y < threshold[0]) | (y > threshold[1]))


def net_benefit(y_true, y_pred, threshold, mode="model", treated=True):
    if isinstance(y_true, pd.Series):
        y_true = y_true.values
    else:
        y_true = y_true.flatten()
    if isinstance(y_true, pd.Series):
        y_pred = y_pred.values
    else:
        y_pred = y_pred.flatten()

    N = len(y_true)
    TN = (y_pred == y_true) & (y_true == 0)
    TP = (y_pred == y_true) & (y_true == 1)
    FN = (y_pred != y_true) & (y_true == 1)
    FP = (y_pred != y_true) & (y_true == 0)

    if treated:
        if mode == "model":
            return sum(TP) / N - sum(FP) / N * threshold / (1 - threshold)
        elif mode == "oracle":
            return np.mean(y_true)
        elif mode == "all":
            return np.mean(y_true) - (1 - np.mean(y_true)) * threshold / (1 - threshold)
        elif mode == "none":
            return 0
    else:
        if mode == "model":
            return sum(TN) / N - sum(FN) / N * (1 - threshold) / threshold
        elif mode == "oracle":
            return 1 - np.mean(y_true)
        elif mode == "all":
            return 1 - np.mean(y_true) - np.mean(y_true) * (1 - threshold) / threshold
        elif mode == "none":
            return 0


def net_cost(
    y_true,
    y_pred,
    threshold,
    cost_tn=-100,
    cost_tp=100,
    cost_fn=200,
    cost_fp=200,
    mode="model",
):
    if isinstance(y_true, pd.Series):
        y_true = y_true.values
    else:
        y_true = y_true.flatten()
    if isinstance(y_true, pd.Series):
        y_pred = y_pred.values
    else:
        y_pred = y_pred.flatten()

    N = len(y_true)
    TN = (y_pred == y_true) & (y_true == 0)
    TP = (y_pred == y_true) & (y_true == 1)
    FN = (y_pred != y_true) & (y_true == 1)
    FP = (y_pred != y_true) & (y_true == 0)

    if mode == "model":
        return (
            (
                sum(TN) * cost_tn
                + sum(TP) * cost_tp
                + sum(FN) * cost_fn
                + sum(FP) * cost_fp
            )
            / (cost_tn + cost_tp + cost_fn + cost_fp)
            / N
        )
    elif mode == "oracle":
        return (
            (sum(y_true) * cost_tn + sum(y_true == 0) * cost_tp)
            / (cost_tn + cost_tp + cost_fn + cost_fp)
            / N
        )
    elif mode == "all":
        return (
            (sum(y_true) * cost_tp + (N - sum(y_true)) * cost_fp)
            / (cost_tn + cost_tp + cost_fn + cost_fp)
            / N
        )
    elif mode == "none":
        return (
            ((N - sum(y_true)) * cost_tn + sum(y_true) * cost_fn)
            / (cost_tn + cost_tp + cost_fn + cost_fp)
            / N
        )


def npv(y_true, y_pred):
    if isinstance(y_true, pd.Series):
        y_true = y_true.values
    else:
        y_true = y_true.flatten()
    if isinstance(y_true, pd.Series):
        y_pred = y_pred.values
    else:
        y_pred = y_pred.flatten()

    TN = (y_pred == y_true) & (y_true == 0)
    TP = (y_pred == y_true) & (y_true == 1)
    FN = (y_pred != y_true) & (y_true == 1)
    FP = (y_pred != y_true) & (y_true == 0)
    return sum(TN) / (sum(TN) + sum(FN)) if sum(TN) > 0 else 0


def specificity(y_true, y_pred):
    if isinstance(y_true, pd.Series):
        y_true = y_true.values
    else:
        y_true = y_true.flatten()
    if isinstance(y_true, pd.Series):
        y_pred = y_pred.values
    else:
        y_pred = y_pred.flatten()

    TN = (y_pred == y_true) & (y_true == 0)
    N = y_true == 0
    return sum(TN) / (sum(N)) if sum(TN) > 0 else 0


def save_plot(name_prefix: str):
    """Save figure to disk as a myriad of file formats."""
    path = Path("figs") / name_prefix
    # Make parent directories if necessary.
    path.parent.mkdir(parents=True, exist_ok=True)
    for suffix in [".eps", ".png", ".pdf", ".svg"]:
        plt.savefig(str(path) + suffix, bbox_inches="tight")
