{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c4d9a268",
   "metadata": {},
   "source": [
    "**Goal**: From the eight previously detected genes (notebook 4b), analyse whether the amount of mutated RNA is more abundant in the durable benefit group.\n",
    "Specifically, we consider the following genes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "0f786da6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Genes to analyse based on analysis 4b.\n",
    "genes = [\n",
    "    'ATM',\n",
    "    'EPHA5',\n",
    "    'LRP1B',\n",
    "    'MTOR',\n",
    "    'NRG1',\n",
    "    'PTPRD',\n",
    "    'PTPRT',\n",
    "    'RUNX1T1',\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "820b7fe1",
   "metadata": {},
   "outputs": [],
   "source": [
    "from glob import glob\n",
    "from tempfile import TemporaryDirectory\n",
    "from typing import Optional\n",
    "import warnings\n",
    "\n",
    "from matplotlib import pyplot as plt\n",
    "from numpy import array\n",
    "import pandas as pd\n",
    "from pandas import DataFrame, Series, concat, read_csv\n",
    "from scipy.stats import fisher_exact, kendalltau, ks_2samp\n",
    "from sklearn.metrics import roc_auc_score\n",
    "from statsmodels.stats.multitest import fdrcorrection\n",
    "from statkit.non_parametric import bootstrap_score\n",
    "from statkit.views import format_p_value\n",
    "\n",
    "from utils import polish_variable_names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "91ccc342",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Add end-to-end package to path.\n",
    "import sys\n",
    "from pathlib import Path\n",
    "\n",
    "sys.path.append(str(Path('../').absolute()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "896e4403",
   "metadata": {},
   "outputs": [],
   "source": [
    "from src.dataset import ARTIFACT_DIR\n",
    "from src.dataset.hartwig import load_hartwigcount\n",
    "from src.feature_extraction.rna import TPM, extract_rna_vcf_spectrum"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "a1d21117",
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = load_hartwig()\n",
    "dataset = polish_variable_names(dataset)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "2158314a",
   "metadata": {},
   "outputs": [],
   "source": [
    "has_y = dataset['Durable benefit'].notna()\n",
    "rna_pass = dataset['rna_quality_pass'] == 1\n",
    "dataset = dataset[rna_pass & has_y]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "557a954c",
   "metadata": {},
   "outputs": [],
   "source": [
    "X_count = read_csv(\"../artifacts/htseq-counts.tsv\", index_col=0, sep=\"\\t\")\n",
    "X_count = X_count[X_count.gene.isin(genes)].drop(columns='gene').sort_index().transpose()\n",
    "\n",
    "# Add HRTW_ prefix.\n",
    "X_count.index = X_count.index.map(lambda x: \"HRTW_\" + x.split(\"_\")[0])\n",
    "# Remove Ensembl gene id suffix .number.\n",
    "# Remove subjects not in our filtered cohort.\n",
    "subject_with_rna = set(X_count.index).intersection(dataset.index)\n",
    "X_count = X_count.loc[list(subject_with_rna)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "5a91ac8f",
   "metadata": {},
   "outputs": [],
   "source": [
    "X_tpm = TPM(\n",
    "    \"../src/feature_extraction/resources/gene_length_gencode.v36.tsv\", aggregate=None\n",
    ").fit_transform(X_count)\n",
    "\n",
    "# Strip Ensemble version suffix.\n",
    "X_tpm.columns = X_tpm.columns.map(lambda x: x.split(\".\")[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "ba4d7daf",
   "metadata": {},
   "outputs": [],
   "source": [
    "def re_estimate_mutated_rna(x_gene: Series, x_vaf: Series, tumor_purity: float) -> Series:\n",
    "    \"\"\"Re-estimate RNA as if it were coming from 100 % tumor purity.\n",
    "    \n",
    "    Returns:\n",
    "        A copy of x_gene containing amount of mutated RNA (restimated by tumor purity). \n",
    "    \"\"\"\n",
    "    # Enrichment: vaf / tumor_purity.\n",
    "    enrichment = Series(0, x_gene.index)\n",
    "    enrichment.loc[x_vaf.index] = x_vaf\n",
    "    enrichment /= tumor_purity\n",
    "\n",
    "    # Estimate amount of mutants per transcript.\n",
    "    x_renormalised = x_gene * enrichment\n",
    "    return x_renormalised\n",
    "\n",
    "def extract_mutated_rna(vcf_pattern: str, X_gene: DataFrame, tumor_purity: Series) -> DataFrame:\n",
    "    \"\"\"Estimate number of mutant transcripts for mutations in VCF file.\n",
    "    \n",
    "    Args:\n",
    "        vcf_pattern: Select all VCF files matching the glob pattern.\n",
    "        X_count: Number of reads covering a gene.\n",
    "        spectrum_filter: Method to filter variants.\n",
    "        tumor_purity: Amount of tumor content of sample, used to \n",
    "            renormalize sample to 100 % purity.\n",
    "    \"\"\"\n",
    "    index = []\n",
    "    features = []\n",
    "\n",
    "    for vcf_file in glob(vcf_pattern):\n",
    "        # For example: CPCT02020429T.rna.vcf => HRTW_CPCT02020429T.\n",
    "        patient_id = 'HRTW_' + vcf_file.split('.rna.vcf')[0].split('/')[-1]\n",
    "\n",
    "        if patient_id not in X_gene.index:\n",
    "            print(f'Skipping {patient_id} (not in count matrix).')\n",
    "            continue\n",
    "        \n",
    "        # Suppress vcfpy warning about empty DP field.\n",
    "        with warnings.catch_warnings():\n",
    "            warnings.simplefilter(\"ignore\")\n",
    "            X_spectrum = extract_rna_vcf_spectrum(vcf_file)\n",
    "\n",
    "            \n",
    "        # Only analyse transcripts where we have count data.\n",
    "        X_spectrum = X_spectrum[X_spectrum.transcript.isin(X_gene.columns)]\n",
    "        if len(X_spectrum) == 0:\n",
    "            print(f'No mutations found in {X_gene.columns} for {patient_id}.')\n",
    "            print('Skipping...')\n",
    "            continue\n",
    "              \n",
    "        x_gene_corrected = re_estimate_mutated_rna(\n",
    "            x_gene=X_gene.loc[patient_id],\n",
    "            x_vaf=X_spectrum.groupby('transcript').mean()['vaf'],\n",
    "            tumor_purity=tumor_purity.loc[patient_id],\n",
    "        )\n",
    "       \n",
    "        index.append(patient_id)\n",
    "        features.append(x_gene_corrected)\n",
    "        \n",
    "    return DataFrame(features, columns=X_gene.columns, index=index)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "0b83222c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02020485T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02010603T.\n",
      "Skipping...\n",
      "Skipping HRTW_CPCT02010455T (not in count matrix).\n",
      "Skipping HRTW_CPCT02010807T (not in count matrix).\n",
      "Skipping HRTW_CPCT02020417T (not in count matrix).\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02080138T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02010451T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02040039T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02040216T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02010356T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02010333TII.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02010924T.\n",
      "Skipping...\n",
      "Skipping HRTW_CPCT02020429T (not in count matrix).\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02010904T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02100156T.\n",
      "Skipping...\n",
      "Skipping HRTW_CPCT02040115T (not in count matrix).\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02010530T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02040092T.\n",
      "Skipping...\n",
      "No mutations found in Index(['ENSG00000079102', 'ENSG00000145242', 'ENSG00000149311',\n",
      "       'ENSG00000153707', 'ENSG00000157168', 'ENSG00000168702',\n",
      "       'ENSG00000196090', 'ENSG00000198793'],\n",
      "      dtype='object', name='transcript') for HRTW_CPCT02120097T.\n",
      "Skipping...\n"
     ]
    }
   ],
   "source": [
    "X_mutated_rna = extract_mutated_rna(\n",
    "    vcf_pattern='../artifacts/vcf_RNA/*.rna.vcf',\n",
    "    X_gene=X_tpm,\n",
    "    tumor_purity=dataset['tumor_purity'],\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "888ccf7c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Number of patients RNA differential gene expression analysis: m = 22\n"
     ]
    }
   ],
   "source": [
    "y = dataset.loc[X_mutated_rna.index, ['Durable benefit']].rename(columns={'Durable benefit': 'durable_benefit'}).replace({'Yes': 1, 'No': 0})\n",
    "Xy = X_mutated_rna.join(y, how=\"inner\").reset_index()\n",
    "Xy.to_csv(ARTIFACT_DIR/'mutated_rna.csv')\n",
    "\n",
    "print(f'Number of patients RNA differential gene expression analysis: m = {len(Xy)}')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f4e5daa",
   "metadata": {},
   "source": [
    "Here, we use the non-parametric KS test instead of, for example, the parametric DESeq2 test. The reason is that  the amount of mutated RNA contains zeroes which DESeq2 can not properly handle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "cbcc3b50",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_significant_genes(data, columns, condition, alternative='two-sided') -> DataFrame:\n",
    "    \"\"\"Test `columns` for significant differences according to `condition`.\"\"\"\n",
    "    result = DataFrame(columns=['statistic', 'pvalue'], index=columns)\n",
    "    for c in columns:\n",
    "        res = ks_2samp(data.loc[~condition, c], data.loc[condition, c], alternative=alternative)\n",
    "        result.loc[c] = [res.statistic, res.pvalue]\n",
    "        \n",
    "    _, pvalue_corrected = fdrcorrection(result.pvalue)\n",
    "    result['pvalue-corrected'] = pvalue_corrected\n",
    "    return result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "b81f160e",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>statistic</th>\n",
       "      <th>pvalue</th>\n",
       "      <th>pvalue-corrected</th>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>transcript</th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "      <th></th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>ENSG00000079102</th>\n",
       "      <td>0.145299</td>\n",
       "      <td>0.997684</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ENSG00000145242</th>\n",
       "      <td>0.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ENSG00000149311</th>\n",
       "      <td>0.145299</td>\n",
       "      <td>0.997684</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ENSG00000153707</th>\n",
       "      <td>0.111111</td>\n",
       "      <td>0.999984</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ENSG00000157168</th>\n",
       "      <td>0.111111</td>\n",
       "      <td>0.999984</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ENSG00000168702</th>\n",
       "      <td>0.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ENSG00000196090</th>\n",
       "      <td>0.0</td>\n",
       "      <td>1.0</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ENSG00000198793</th>\n",
       "      <td>0.230769</td>\n",
       "      <td>0.871501</td>\n",
       "      <td>1.0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                statistic    pvalue pvalue-corrected\n",
       "transcript                                          \n",
       "ENSG00000079102  0.145299  0.997684              1.0\n",
       "ENSG00000145242       0.0       1.0              1.0\n",
       "ENSG00000149311  0.145299  0.997684              1.0\n",
       "ENSG00000153707  0.111111  0.999984              1.0\n",
       "ENSG00000157168  0.111111  0.999984              1.0\n",
       "ENSG00000168702       0.0       1.0              1.0\n",
       "ENSG00000196090       0.0       1.0              1.0\n",
       "ENSG00000198793  0.230769  0.871501              1.0"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "get_significant_genes(X_mutated_rna, columns=X_mutated_rna.columns, condition=(y['durable_benefit'] == 1))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  },
  "vscode": {
   "interpreter": {
    "hash": "916dbcbb3f70747c44a77c7bcd40155683ae19c65e1c03b4aa3499c5328201f1"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
