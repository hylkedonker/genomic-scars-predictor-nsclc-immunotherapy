#!/bin/bash
# Perform liftover (or, remapping) of VCF files from reference genome GRCh37 (hg19)
# to GRCh38 (hg38).
#
# Requires: 
# - CrossMap (`pip3 install CrossMap`)
# - GRCh37_to_GRCh38.chain.gz
# - GRCh38.p13.genome.fa
target_dir=../artifacts/vcf_GRCh38
source_dir=../artifacts/vcf
mkdir -p ${target_dir}

GRCh38=../resources/GRCh38.p13.genome.fa
for vcf_file in ${source_dir}/*.vcf; do
    name=$(basename ${vcf_file})
    echo CrossMap.py  vcf ../resources/GRCh37_to_GRCh38.chain.gz ${vcf_file} ${GRCh38} "${target_dir}/${name}"
    CrossMap.py  vcf ../resources/GRCh37_to_GRCh38.chain.gz ${vcf_file} ${GRCh38} "${target_dir}/${name}"
done
