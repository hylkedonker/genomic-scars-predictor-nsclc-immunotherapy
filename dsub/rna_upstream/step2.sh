#!/bin/bash
TrimmomaticPE \
    -trimlog ${LOG} \
    -phred33 \
    "${R1}" \
    "${R2}" \
    "${R1_paired}" \
    "${R1_unpaired}" \
    "${R2_paired}" \
    "${R2_unpaired}" \
    ILLUMINACLIP:TruSeq3-PE.fa:2:30:10 \
    LEADING:3 \
    TRAILING:3 \
    SLIDINGWINDOW:4:25 \
    HEADCROP:8 \
    MINLEN:50