#!/bin/bash
samtools index \
    -@ 2 \
    ${BAM} \
    "${BAM}.bai"
htseq-count \
    --stranded=reverse \
    --order pos \
    --format bam \
    --idattr gene_id \
    --additional-attr=gene_name \
    --type gene \
    -n 2 \
    ${BAM} \
    ${GTF_FILE} \
    -c ${COUNTS}
