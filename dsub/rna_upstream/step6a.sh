#!/bin/bash
samtools index \
    -@ 2 \
    ${BAM} \
    "${BAM}.bai"
htseq-count \
    --stranded=reverse \
    --order pos \
    --format bam \
    --idattr gene_id \
    --type exon \
    -n 2 \
    ${BAM} \
    ${GTF_FILE} \
    -c ${COUNTS}
