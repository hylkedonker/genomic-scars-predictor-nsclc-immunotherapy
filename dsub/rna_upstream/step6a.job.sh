#!/bin/bash
# Requires the file step6.tsv with the following fields pointing to a google storage
# bucket location:
#	--input BAM
#	--output COUNTS
#
# and the genome annotation:
# - ${REFDATA}/gencode.v36.chr_patch_hapl_scaff.annotation.gff3
#
BUCKET='<censored>'
REFDATA="${BUCKET}/refdata"
# Step 6
# Compute unnormalised counts per _EXON_ using gencode annotation.
dsub \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --min-ram 20 \
    --timeout 2d \
    --input GTF_FILE=${REFDATA}/gencode.v36.chr_patch_hapl_scaff.annotation.gff3 \
    --logging "${BUCKET}/logging/htseq/step6/" \
    --image eu.gcr.io/<censored>/rna-seq:light \
    --tasks step6.tsv \
    --script step6a.sh
