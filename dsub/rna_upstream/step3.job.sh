#!/bin/bash
# Requires the file step3.tsv with the following fields pointing to a google storage
# bucket location:
#	--input R1
#	--input R2
#	--output-recursive FASTQC_OUT
#
BUCKET='<censored>'
# Step 3
# Run FastQC again after trimming.
dsub \
    --preemptible \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --logging "${BUCKET}/logging/RNAseq/step3/" \
    --image eu.gcr.io/<censored>/rna-seq:fastqc \
    --tasks ./step3.tsv \
    --script step3.sh
