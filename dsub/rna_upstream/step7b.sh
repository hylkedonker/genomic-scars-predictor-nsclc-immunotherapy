#!/bin/bash
python3 /pipeline/tpm.py ${COUNTS} \
    ${GENE_LENGTH_FILE} \
    ${RAW} \
    ${TPM} \
    ${COUNT_METADATA}