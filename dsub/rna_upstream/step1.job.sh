#!/bin/bash
# Requires the file step1.tsv with the following fields pointing to a google storage
# bucket location:
#   --input R1
#   --input R2
#   --output-recursive FASTQC_OUT
#
BUCKET='<censored>'
# Step 1
# Run FastQC.
dsub \
    --preemptible \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --logging "${BUCKET}/logging/RNAseq/step1/" \
    --image eu.gcr.io/<censored>/rna-seq:fastqc \
    --tasks ./step1.tsv \
    --script step1.sh