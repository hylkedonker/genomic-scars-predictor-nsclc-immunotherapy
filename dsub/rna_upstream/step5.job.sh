#!/bin/bash
BUCKET='<censored>'
OUTPUT="${BUCKET}/output/RNAseq"
# Step 5
# Compile quality control metrics.
dsub \
    --preemptible \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --input-recursive STEP2="${OUTPUT}/step2/" \
    --input-recursive STEP3="${OUTPUT}/step3/" \
    --input-recursive STEP4="${OUTPUT}/step4/" \
    --output-recursive OUTPUT="${OUTPUT}/step5/" \
    --logging "${BUCKET}/logging/RNAseq/step5/" \
    --image ewels/multiqc \
    --script step5.sh
