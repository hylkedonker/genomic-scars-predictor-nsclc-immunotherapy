#!/bin/bash
# Requires the file step4.tsv with the following fields pointing to a google storage
# bucket location:
#	--input R1
#	--input R2
#	--output-recursive STAR_OUTPUT
#
BUCKET='<censored>'
REFDATA="${BUCKET}/refdata"
GENOME_INDEX="${BUCKET}/output/RNAseq/step4a/genome_index"
# Step 4
# Align reads against reference genome.
dsub \
    --preemptible \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --min-cores 8 \
    --min-ram 40 \
    --timeout 2d \
    --logging "${BUCKET}/logging/RNAseq/step4b/" \
    --input-recursive GENOME_INDEX="${GENOME_INDEX}" \
    --image eu.gcr.io/<censored>/rna-seq:light \
    --tasks step4.tsv \
    --script step4b.sh
