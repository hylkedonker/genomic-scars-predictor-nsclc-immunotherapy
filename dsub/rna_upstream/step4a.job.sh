#!/bin/bash
BUCKET='<censored>'

# Requires the files pointing to a google storage bucket location:
# - ${REFDATA}/GRCh38.p13.genome.fa
# - ${REFDATA}/gencode.v36.chr_patch_hapl_scaff.annotation.gff3
REFDATA="${BUCKET}/refdata"

GENOME_INDEX="${BUCKET}/output/RNAseq/step4a/genome_index"

dsub \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --preemptible \
    --min-ram 48 \
    --min-cores 8 \
    --logging "${BUCKET}/logging/RNAseq/step4a/" \
    --input FASTA_FILE=${REFDATA}/GRCh38.p13.genome.fa \
    --input GTF_FILE=${REFDATA}/gencode.v36.chr_patch_hapl_scaff.annotation.gff3 \
    --output-recursive GENOME_INDEX=${GENOME_INDEX} \
    --image eu.gcr.io/<censored>/rna-seq:light \
    --script step4a.sh