#!/bin/bash
echo "Output dir: ${STAR_OUTPUT}/"
STAR \
    --runThreadN 8 \
    --readFilesCommand zcat \
    --readFilesIn "${R1}" "${R2}" \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMattrIHstart 0 \
    --alignSoftClipAtReferenceEnds No \
    --outFilterIntronMotifs RemoveNoncanonical \
    --genomeDir ${GENOME_INDEX} \
    --outFileNamePrefix "${STAR_OUTPUT}/"
echo "ls ${STAR_OUTPUT}/"
ls ${STAR_OUTPUT}/