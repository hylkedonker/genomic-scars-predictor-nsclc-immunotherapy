# Compute TPM for samples.
#
# Usage:
# tpm.py /path/to/htseq/samples \
#   /path/to/gene/length \
#   /output/tpm.tsv \
#   /output/count_metadata.tsv
#
from glob import glob
import sys

import pandas as pd


BASE_PATH = sys.argv[1]
GENE_LENGTH = sys.argv[2]
RAW_OUTPUT = sys.argv[3]
TPM_OUTPUT = sys.argv[4]
METADATA_OUTPUT = sys.argv[5]


samples = glob(f"{BASE_PATH}/*/counts.txt")
gene_size = (
    pd.read_csv(GENE_LENGTH, sep="\t", index_col=0).groupby("gene").sum().squeeze()
)

counts = {}
for sample in sorted(samples):
    sample_name = sample.lstrip(BASE_PATH).rstrip("/counts.txt")
    counts[sample_name] = (
        pd.read_csv(
            sample, sep="\t", names=["transcript", "gene", "count"], index_col=0
        )
        .reset_index()
        .set_index(["transcript", "gene"])
        .squeeze()
    )

counts = pd.DataFrame(counts)
meta_indices = [
    "__no_feature",
    "__ambiguous",
    "__too_low_aQual",
    "__not_aligned",
    "__alignment_not_unique",
]

count_metadata = counts.loc[meta_indices].copy()
counts = counts.drop(index=meta_indices).sort_index()
mapped_fraction = counts.sum(axis=0) / (
    count_metadata.loc["__no_feature"] + counts.sum(axis=0)
)
count_metadata.to_csv(METADATA_OUTPUT, sep="\t")

print("Amount of transcripts mapped to features")
print(mapped_fraction)

# Store raw counts.
counts.to_csv(RAW_OUTPUT, sep="\t")
# Compute TPM.
counts = counts.groupby("gene").sum()
normalised_counts = counts.divide(gene_size, axis=0)
tpm = normalised_counts / normalised_counts.sum(axis=0) * 10 ** 6
tpm.to_csv(TPM_OUTPUT, sep="\t")
