#!/bin/bash
# Requires the file step0.tsv with the following fields pointing to a google storage
# bucket location:
#	--input FASTQ_LANE_1
#	--input FASTQ_LANE_2
#	--input FASTQ_LANE_3
#	--input FASTQ_LANE_4
#    --output FASTQ_OUTPUT
#
BUCKET='<censored>'
dsub \
    --provider google-cls-v2 \
    --preemptible \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --logging "${BUCKET}/logging/RNAseq/step0/" \
    --tasks ./step0.tsv \
    --script step0.sh