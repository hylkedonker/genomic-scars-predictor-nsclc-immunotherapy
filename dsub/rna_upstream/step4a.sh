#!/bin/bash
STAR --runThreadN 8 \
    --runMode genomeGenerate \
    --sjdbOverhang 100 \
    --genomeFastaFiles ${FASTA_FILE} \
    --sjdbGTFfile ${GTF_FILE} \
    --genomeDir ${GENOME_INDEX}