#!/bin/bash
# Requires the genome annotation:
# - ${REFDATA}/gencode.v36.chr_patch_hapl_scaff.annotation.gff3
#
BUCKET='<censored>'
REFDATA="${BUCKET}/refdata"
# Step 7
# a) Compute gene lengths for TPM computation.
dsub \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --timeout 2d \
    --input GTF_FILE=${REFDATA}/gencode.v36.chr_patch_hapl_scaff.annotation.gff3 \
    --output GENE_LENGTH_FILE=${REFDATA}/gene_length_gencode.v36.tsv \
    --logging "${BUCKET}/logging/htseq/step7a/" \
    --image eu.gcr.io/<censored>/rna-seq:light \
    --script step7a.sh
