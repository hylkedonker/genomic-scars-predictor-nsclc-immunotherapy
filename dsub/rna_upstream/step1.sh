#!/bin/bash
fastqc \
    -o "${FASTQC_OUT}" \
    --threads 2 \
    "${R1}" "${R2}"