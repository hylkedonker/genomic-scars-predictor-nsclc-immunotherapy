#!/bin/bash
# For each mate (L and R), of each sample, concatenate (`cat`) FASTQ lanes.
cat \
    ${FASTQ_LANE_1} \
    ${FASTQ_LANE_2} \
    ${FASTQ_LANE_3} \
    ${FASTQ_LANE_4} \
    > ${FASTQ_OUTPUT}