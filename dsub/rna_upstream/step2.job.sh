#!/bin/bash
# Requires the file step2.tsv with the following fields pointing to a google storage
# bucket location:
#	--input R1
#	--input R2
#	--output R1_paired
#	--output R1_unpaired
#	--output R2_paired
#	--output R2_unpaired
#	--output LOG
#
BUCKET='<censored>'
# Step 2
# Trim paired-end reads.
dsub \
    --preemptible \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --logging "${BUCKET}/logging/RNAseq/step2/" \
    --image eu.gcr.io/<censored>/rna-seq:trimmomatic \
    --tasks ./step2.tsv \
    --script step2.sh