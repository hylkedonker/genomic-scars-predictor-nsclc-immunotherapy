#!/usr/bin/python3
# Count number of mutant reads at positions specified in VCF file.
#
# Usage:
#
# python3 rna.py input/rna.bam input/variants_dna.vcf out/coverage_rna.vcf
from collections import defaultdict

import pysam
import vcfpy

import sys


def get_coverage(bam_path, chromosome, position, size: int = 1) -> dict[str, int]:
    """Extract coverage per motif at given genomic position.

    Args:
        size: Compute motif coverage of this size (e.g., size=1 single base
            substitution and size=2 for doublet base substitutions).
    """
    nucleotides: dict[str, int] = defaultdict(int)
    with pysam.AlignmentFile(bam_path, "rb") as samfile:
        for pile in samfile.pileup(
            contig=chromosome,
            start=position,
            end=position + size,
            truncate=False,
            ignore_overlaps=True,
        ):
            # We are only interested in the bases on `start_pos`.
            if pile.pos != position - 1:
                continue

            for read in pile.pileups:
                if read.is_del or read.is_refskip:
                    continue

                # In case we are looking for double base substitutions, allow
                # for more than one nucleotide.
                motif = read.alignment.query_sequence[
                    read.query_position : read.query_position + size
                ]

                nucleotides[motif] += 1
    return nucleotides


def allele_frequency(alt_depth, total_depth):
    if alt_depth > 0:
        return alt_depth / total_depth
    return 0


def coverage_to_vcf(bam_path, input_vcf, output_vcf):
    """
    Compute allele frequency and total depth from BAM and write to VCF.

    Args:
        bam_path: Extract coverage from this sequence alignment map.
        input_vcf: Only analyse coverage for these variants.
        output_vcf: Write total coverage depth (`DP`) and allele frequency
            (`AF`) to this file.
    """
    with vcfpy.Reader.from_path(str(input_vcf)) as reader:
        reader.header.add_info_line(
            {
                "ID": "AF",
                "Number": "A",
                "Type": "Float",
                "Description": "Allele Frequency",
            }
        )
        with vcfpy.Writer.from_path(output_vcf, reader.header) as writer:
            for record in reader:
                chrom = record.CHROM
                if not chrom.startswith("chr"):
                    chrom = "chr" + chrom

                coverage = get_coverage(
                    bam_path, chrom, record.POS, size=len(record.REF)
                )

                total_depth = sum(coverage.values())
                record.INFO["DP"] = total_depth
                vaf = [
                    allele_frequency(coverage[alt.value], total_depth)
                    for alt in record.ALT
                ]
                record.INFO["AF"] = vaf
                writer.write_record(record)
                print(".", end="")


coverage_to_vcf(*sys.argv[1:])
