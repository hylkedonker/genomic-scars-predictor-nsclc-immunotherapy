#!/bin/bash
BUCKET='<censored>'
# Step 2
# Count RNA coverage at mutated sites.
dsub \
    --preemptible \
    --provider google-cls-v2 \
    --project <censored> \
    --location europe-west4 \
    --zones europe-west4-a \
    --logging "${BUCKET}/logging/DNA-RNAseq/step2/" \
    --image eu.gcr.io/<censored>/vcfpy \
    --tasks ./step2.tsv \
    --script step2.sh